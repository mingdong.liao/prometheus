// Copyright 2015 The Prometheus Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package qcloud

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/prometheus/common/model"
	"github.com/prometheus/prometheus/discovery/targetgroup"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/regions"
)

func TestDesc(t *testing.T) {
	id := os.Getenv("TENCENTCLOUD_SECRET_ID")
	key := os.Getenv("TENCENTCLOUD_SECRET_KEY")
	if len(id) == 0 || len(key) == 0 {
		fmt.Printf("please specify env TENCENTCLOUD_SECRET_ID && TENCENTCLOUD_SECRET_KEY\n")
		return
	}

	conf := DefaultSDConfig

	conf.Port = 1222
	conf.AccessKey = id
	conf.SecretKey = key
	conf.BatchNum = 23
	conf.Region = regions.Guangzhou
	conf.RefreshInterval = model.Duration(3 * time.Second)

	conf.Filters = []*Filter{
		&Filter{
			Name:   "instance-name",
			Values: []string{"ssp-test", "ssp-test-mixer"},
		},
		&Filter{
			Name:   "zone",
			Values: []string{"ap-guangzhou-3", "ap-guangzhou-4"},
		},
	}

	d := NewDiscovery(&conf, nil)
	grp, err := d.refresh(context.TODO())

	if err != nil {
		fmt.Printf("tx cloud refresh failed, err:%s\n", err.Error())
		return
	}

	fmt.Printf("result, num:%d, value:%v\n", len(grp.Targets), grp.Targets)

	ch := make(chan []*targetgroup.Group)
	go d.Run(context.TODO(), ch)

	select {
	case grp2 := <-ch:
		fmt.Printf("result, num:%d, value:%v\n", len(grp2[0].Targets), grp2[0].Targets)
	}
}
