// Copyright 2015 The Prometheus Authors
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package qcloud

import (
	"context"
	"fmt"
	"net"
	"strings"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/model"
	"github.com/prometheus/prometheus/discovery/targetgroup"

	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/regions"
	cvm "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/cvm/v20170312"
)

const (
	txLabel              = model.MetaLabelPrefix + "tx_"
	txLabelZone          = txLabel + "zone"
	txLabelInstanceId    = txLabel + "instance_id"
	txLabelInstanceName  = txLabel + "instance_name"
	txLabelInstanceType  = txLabel + "instance_type"
	txLabelInstanceState = txLabel + "instance_state"
	txLabelPlatform      = txLabel + "platform"
	txLabelPublicIP      = txLabel + "public_ip"
	txLabelPrivateIP     = txLabel + "private_ip"
	txLabelTag           = txLabel + "tag_kv"
)

var (
	txSDRefreshFailuresCount = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "prometheus_sd_tx_refresh_failures_total",
			Help: "The number of tx-SD scrape failures.",
		})

	txSDRefreshDuration = prometheus.NewSummary(
		prometheus.SummaryOpts{
			Name: "prometheus_sd_tx_refresh_duration_seconds",
			Help: "The duration of a tx-SD refresh in seconds.",
		})

	DefaultSDConfig = SDConfig{
		Port:            9100,
		ReqMethod:       "Post",
		BatchNum:        30,
		RefreshInterval: model.Duration(60 * time.Second),
	}
)

type Filter struct {
	Name   string   `yaml:"name"`
	Values []string `yaml:"values"`
}

type SDConfig struct {
	Endpoint        string         `yaml:"endpoint"`
	Region          string         `yaml:"region"`
	AccessKey       string         `yaml:"secret_id,omitempty"`
	SecretKey       string         `yaml:"secret_key,omitempty"`
	ReqMethod       string         `yaml:"method"`
	RefreshInterval model.Duration `yaml:"refresh_interval,omitempty"`
	Port            int            `yaml:"port"`
	BatchNum        int            `yaml:"batch_num,omitempty"`
	Filters         []*Filter      `yaml:"filters"`
}

type Discovery struct {
	config SDConfig
	client *cvm.Client
	logger log.Logger
}

func init() {
	prometheus.MustRegister(txSDRefreshDuration)
	prometheus.MustRegister(txSDRefreshFailuresCount)
}

func (c *SDConfig) UnmarshalYAML(unmarshal func(interface{}) error) error {
	*c = DefaultSDConfig
	type plain SDConfig
	err := unmarshal((*plain)(c))
	if err != nil {
		return err
	}
	if c.Region == "" {
		c.Region = regions.Guangzhou
	}

	if c.Endpoint == "" {
		c.Endpoint = "cvm.tencentcloudapi.com"
	}

	if c.ReqMethod == "" {
		c.ReqMethod = "Post"
	}

	if len(c.AccessKey) == 0 || len(c.SecretKey) == 0 {
		return fmt.Errorf("qcloud secret_id and secret_key can not be empty.")
	}

	return nil
}

func NewDiscovery(conf *SDConfig, logger log.Logger) *Discovery {
	if logger == nil {
		logger = log.NewNopLogger()
	}

	creds := common.NewCredential(conf.AccessKey, conf.SecretKey)
	cpf := profile.NewClientProfile()

	cpf.SignMethod = "HmacSHA1"
	cpf.HttpProfile.ReqTimeout = 10
	cpf.HttpProfile.Endpoint = conf.Endpoint
	cpf.HttpProfile.ReqMethod = conf.ReqMethod

	client, err := cvm.NewClient(creds, conf.Region, cpf)
	if err != nil {
		level.Error(logger).Log("msg", "create tx client failed", "err", err)
	}

	return &Discovery{
		config: *conf,
		client: client,
		logger: logger,
	}
}

func (d *Discovery) Run(ctx context.Context, ch chan<- []*targetgroup.Group) {
	ticker := time.NewTicker(time.Duration(d.config.RefreshInterval))
	defer ticker.Stop()

	// get first time
	tg, err := d.refresh(ctx)
	if err != nil {
		level.Error(d.logger).Log("msg", "Tx Refresh failed", "err", err)
	} else {
		select {
		case ch <- []*targetgroup.Group{tg}:
		case <-ctx.Done():
			return
		}
	}

	for {
		select {
		case <-ticker.C:
			tg, err := d.refresh(ctx)
			if err != nil {
				level.Error(d.logger).Log("msg", "tx Refresh failed", "err", err)
				continue
			}

			select {
			case ch <- []*targetgroup.Group{tg}:
			case <-ctx.Done():
				return
			}

		case <-ctx.Done():
			return
		}
	}
}

func (d *Discovery) refresh(ctx context.Context) (tg *targetgroup.Group, err error) {
	t0 := time.Now()
	defer func() {
		txSDRefreshDuration.Observe(time.Since(t0).Seconds())
		if err != nil {
			txSDRefreshFailuresCount.Inc()
		}
	}()

	port := fmt.Sprintf("%d", d.config.Port)

	var filters []*cvm.Filter
	for _, f := range d.config.Filters {
		filters = append(filters,
			&cvm.Filter{Name: common.StringPtr(f.Name), Values: common.StringPtrs(f.Values)})
	}

	offset := int64(0)
	tg = &targetgroup.Group{Source: d.config.Region}

	for {
		var response *cvm.DescribeInstancesResponse

		request := cvm.NewDescribeInstancesRequest()

		request.Filters = filters
		request.Offset = common.Int64Ptr(offset)
		request.Limit = common.Int64Ptr(int64(d.config.BatchNum))

		response, err = d.client.DescribeInstances(request)

		if err != nil {
			return
		}

		total := *response.Response.TotalCount
		num := len(response.Response.InstanceSet)

		//fmt.Printf("total:%d, off:%d, cur num:%d\n", total, offset, num)

		for _, inst := range response.Response.InstanceSet {
			if len(inst.PrivateIpAddresses) == 0 {
				continue
			}

			labels := model.LabelSet{
				txLabelZone:          model.LabelValue(*inst.Placement.Zone),
				txLabelInstanceId:    model.LabelValue(*inst.InstanceId),
				txLabelInstanceName:  model.LabelValue(*inst.InstanceName),
				txLabelInstanceType:  model.LabelValue(*inst.InstanceType),
				txLabelInstanceState: model.LabelValue(*inst.InstanceState),
				txLabelPlatform:      model.LabelValue(*inst.OsName),
			}

			if inst.Tags != nil {
				tag := []string{}
				for _, v := range inst.Tags {
					if v == nil || v.Key == nil || v.Value == nil {
						continue
					}
					tv := *(v.Key) + ":" + *(v.Value)
					tag = append(tag, tv)
				}

				labels[txLabelTag] = model.LabelValue(strings.Join(tag, ","))
			}

			if len(inst.PublicIpAddresses) != 0 {
				addr := make([]string, 0, len(inst.PublicIpAddresses))
				for _, v := range inst.PublicIpAddresses {
					if len(*v) == 0 {
						continue
					}
					addr = append(addr, *v)
				}

				if len(addr) > 0 {
					labels[txLabelPublicIP] = model.LabelValue(strings.Join(addr, ","))
				}
			}

			if len(inst.PrivateIpAddresses) != 0 {
				addr := make([]string, 0, len(inst.PrivateIpAddresses))
				for _, v := range inst.PrivateIpAddresses {
					if len(*v) == 0 {
						continue
					}
					addr = append(addr, *v)
				}

				if len(addr) > 0 {
					primary := net.JoinHostPort(addr[0], port)
					labels[model.AddressLabel] = model.LabelValue(primary)
					labels[txLabelPrivateIP] = model.LabelValue(strings.Join(addr, ","))
				}
			}

			tg.Targets = append(tg.Targets, labels)
		}

		offset += int64(num)
		if offset >= total {
			break
		}
	}

	return tg, nil
}
